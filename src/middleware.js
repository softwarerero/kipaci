import fs from 'fs'
import stringify from 'json-stringify-safe'

export const checkAuth = (req, res, next) => {
  console.log('checkAuth', req.headers['x-gitlab-token'])
  console.log('GITLAB_SECRET', process.env.GITLAB_SECRET, req.headers['x-gitlab-token'] === process.env.GITLAB_SECRET)
  req.headers['x-gitlab-token'] === process.env.GITLAB_SECRET ? next() : res.redirect("/")
}

export const evaluateBody = (req, res, next) => {
//   console.log('checkAuth', req.headers['x-gitlab-token'])
//   console.log('GITLAB_SECRET', process.env.GITLAB_SECRET, req.headers['x-gitlab-token'] === process.env.GITLAB_SECRET)
  // req.headers['x-gitlab-token'] === process.env.GITLAB_SECRET ? next() : res.redirect("/")
  // console.log('body', req.body)
}

export const logRoute = (req, res, next) => {
  fs.appendFile(`${process.env.APP_NAME}_routes.log`, stringify(req.body, null, 2), function (error) {
    if (error) console.error(error)
  })
  
  next()
}