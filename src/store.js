import fs from 'fs'
import { constants } from 'perf_hooks'

let store = {}

export const getStore = () => {
  if (!Object.keys(store).length) {
    const deployPath = process.env.DEPLOY_PATH
    fs.mkdirSync(deployPath, { recursive: true })
    if (fs.existsSync(getStorePath())) {
      const text = fs.readFileSync(getStorePath(), 'utf-8')
      store = text ? JSON.parse(text) : {}
    }
  }
  return store
}

export const saveProcess = params => {
  const store = getStore()
  store[params.source_branch] = params
  fs.writeFileSync(getStorePath(), JSON.stringify(store, null, 2))
}

export const removeProcess = branch => {
  const store = getStore()
  delete store[branch]
  fs.writeFileSync(getStorePath(), JSON.stringify(store, null, 2))
}

export const getProcess = branch => getStore()[branch]

const getStorePath = () => `${process.env.DEPLOY_PATH}/store.json`