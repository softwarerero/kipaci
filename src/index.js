import express from 'express'
import dotenv from 'dotenv'
import { checkAuth, logRoute } from './middleware.js'
import { processMergeRequest } from './actions.js'
import { initProxy } from './proxy.js'

const app = express()
app.use(express.json())
const config = dotenv.config()
// console.log(config)

app.get('/', (req, res) => {
  // console.log(req)
  res.send('I will be my brother\'s KiPa!')
})
  
// A merge request has been created
app.post('/mr', [checkAuth, logRoute], (req, res) => {
  // console.log(req)
  processMergeRequest(req.body)
  res.send('Thank you from mr handler!')
})

app.listen(process.env.EXPRESS_PORT)
console.log(`${process.env.APP_NAME} started on port ${process.env.EXPRESS_PORT}`)

initProxy()

import { postToSlack } from './slack.js'

// const message = `Started Meteor instance on ${process.env.URL}:1
//   title: Title
//   description: description
//   source_branch: source_branch
//   url: url
// `
// console.log(message)
// postToSlack(message)

// processMergeRequest({ 
//   project: {
//     id: '20489642',
//   },
//   object_attributes: {
//     projectId: '20489642',
//     created_at: '2021-05-03T13:47:14.064Z',
//     title: 'Deploye merge requests auf eine eigene Instanz',
//     description: 'Closes #344',
//     source_branch: 'feat/344-kipaci',
//     target_branch: 'master',
//     url: 'https://gitlab.com/biblemissions/kipa/-/merge_requests/279',
//     work_in_progress: false,
//     state: 'opened',
//     // state: 'closed',
//     port: "21002",
//   }
// })
