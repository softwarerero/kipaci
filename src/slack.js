import SlackApi from '@slack/web-api'

let slack

export const postToSlack = (text) => {
  (async () => {
    if (!slack) slack = new SlackApi.WebClient(process.env.SLACK_TOKEN)
    // Find more arguments and details of the response: https://api.slack.com/methods/chat.postMessage
    const result = await slack.chat.postMessage({ text, channel: process.env.SLACK_CHANNEL })
    console.log(`Successfully send message ${result.ts} in conversation ${process.env.SLACK_CHANNEL}`)
  })()
}
