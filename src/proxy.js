// import { redbird } from 'redbird'
import redbird from 'redbird'

// console.log({ port: process.env.PROXY_PORT })
// export const proxy = redbird({ port: process.env.PROXY_PORT })  
export let proxy

export const initProxy = () => {
  proxy = redbird({ port: process.env.PROXY_PORT })
  // proxy = redbird({ port: process.env.EXPRESS_PORT })
//   proxy.register(`https://kipa.biblemissions.org:${process.env.PROXY_PORT}`, `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register(`https://kipa.biblemissions.org:${process.env.PROXY_PORT}/home`, `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register('https://kipa.biblemissions.org', `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register(`https://a.kipa.biblemissions.org`, `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register(`https://www.stage.kipa.help`, `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register(`http://www.stage.kipa.help`, `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register(`https://a.kipa.help`, `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register(`https://21004.kipa.help`, `https://kipa.biblemissions.org:21004`)
  // proxy.register(`https://t001.kipa.help`, `https://kipa.biblemissions.org:21004`)
  // proxy.register(`https://stage.kipa.help`, `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  // proxy.register(`https://t001.stage.kipa.help`, `http://kipa.biblemissions.org:21001`)
  // proxy.register(`https://t002.stage.kipa.help`, `http://kipa.biblemissions.org:21002`)
  // proxy.register(`https://t202.kipa.help`, `45.142.176.36:21002`)
  // proxy.register(`https://t003.stage.kipa.help`, `http://kipa.biblemissions.org:21003`)
  // proxy.register(`https://t004.stage.kipa.help`, `http://kipa.biblemissions.org:21004`)
  // proxy.register(`http://t004.stage.kipa.help`, `http://kipa.biblemissions.org:21004`)
  // proxy.register(`t004.stage.kipa.help`, `http://kipa.biblemissions.org:21004`)
//   proxy.register('https://webhooks.kipa.biblemissions.org', `https://kipa.biblemissions.org:${process.env.EXPRESS_PORT}`)
  proxy.register(`https://t002.biblemissions.org`, `http://kipa.biblemissions.org:21002`)
  console.log(`proxy tarted on port ${process.env.PROXY_PORT}`)
}
