# KiPaCI

Continuous Integration for KiPa

The testers of KiPa (child sponsorship administration) should be able to try out new features or fixes whenever a developer creates a new merge request for a branch tagged as `feature` or `fix` or pushes to one of those branches on GitLab.

KiPaCI should check out or pull the changes from the corresponding branch into a new directory, install all dependencies (npm packages) and spin up a new instance. KiPa is build on Meteor so the correct Meteor version should be installed if needed. A free port should be assigned to the instance and a new MongoDB instance should be allocated. A reverse proxy should redirect a URL which starts with the issue number to the port assigned to the Meteor app. SSL should work.

# Doku
https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#merge-request-events
https://github.com/OptimalBits/redbird
http://expressjs.com/en/4x/api.html
https://nodejs.org/api/child_process.html#child_process_child_process_spawn_command_args_options
