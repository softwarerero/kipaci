import fs from 'fs'
import { execSync, spawn, spawnSync } from 'child_process'
import { getStore, saveProcess, getProcess, removeProcess } from './store.js'
// import { initProxy } from './proxy.js'
import { postToSlack } from './slack.js'

export const processMergeRequest = (body) => {
  // console.log('processMergeRequest', body)
  const { work_in_progress, state } = body.object_attributes
  // const { created_at, description, source_branch, target_branch, title, url, work_in_progress, state } = body.object_attributes
  const projectId = body.project.id
  // console.log({ created_at, description, source_branch, target_branch, title, url, work_in_progress, state, projectId })
  console.log({ work_in_progress, state, projectId })
  if (work_in_progress) return // ignore draft MRs
  // TODO check for branch names (starting with feat/fix)
  if (state === 'opened') {
    startMeteor(projectId, body.object_attributes)
  } else if (state === 'update') {
    restartMeteor(projectId, body.object_attributes)
  } else if (state === 'closed') {
    stopMeteor(projectId, body.object_attributes)
  }
}

const startMeteor = (projectId, attributes, port = getFreePort()) => {
  console.log('startMeteor', projectId, attributes.source_branch)
  if (!port) return console.error('No free port available')
  const branch = attributes.source_branch
  const deployPath = process.env.DEPLOY_PATH
  fs.mkdirSync(deployPath, { recursive: true })
  const targetPath = `${deployPath}/${projectId}/${branch}`
  doExecSync(`rm -rf ${targetPath}`)
  doExecSync(`git clone --branch ${branch} ${process.env.GITLAB_REPO} ${targetPath}`)
  doExecSync('meteor npm install', { cwd: targetPath })
  // const args = ['--port', port, '--settings', 'settings-ci.json', '--exclude-archs', 'web.browser.legacy', '2>&1 > output.log']
  const args = ['--port', port, '--settings', 'settings-ci.json', '--exclude-archs', 'web.browser.legacy']
  const { SHELL, PWD, HOME, PATH } = process.env
  const processId = doSpawnAsync('meteor', args, { cwd: targetPath, env: {
    // ...process.env,
    SHELL, PWD, HOME, PATH,
    MAIL_URL: 'smtp://smtp.kipa@biblemissions.org:Tn4bV3xs8TrnNhr9Lq@smtp.office365.com:587',
    MONGO_URL:`mongodb://localhost:27017/${getDbName(branch)}`,
  }})
  const { created_at, description, source_branch, target_branch, title, url, work_in_progress, state } = attributes
  port && saveProcess({ created_at, description, source_branch, target_branch, title, url, work_in_progress, state, port, projectId })
  const message = `Started Meteor instance on ${process.env.URL}:${port}
    title: ${title}
    description: ${description}
    source_branch: ${source_branch}
    url: ${url}
  `
  postToSlack(message)
  console.log(`Started Meteor instance on ${process.env.URL}:${port}`)
  return port
}

const restartMeteor = (projectId, attributes) => {
  console.log('restartMeteor', projectId, attributes)
  const port = stopMeteor(projectId, attributes)
  return startMeteor(projectId, attributes, port)
}

const stopMeteor = (projectId, attributes) => {
  const branch = attributes.source_branch
  console.log('stopMeteor', projectId, branch)
  const processData = getProcess(branch)
  const port = processData && processData.port
  const spawnResult = spawnSync('lsof', ['-t', `-itcp:${port}`])
  const processId = spawnResult.stdout.toString().replace('\n', '')
  if (processId) {
    doExecSync(`kill -9 ${processId}`)
  }
  removeProcess(branch)
  doExecSync(`rm -rf ${process.env.DEPLOY_PATH}/${projectId}/${branch}`)
  if (attributes.state === 'closed' || attributes.state === 'close') {
    doExecSync(`mongo ${getDbName(branch)} --eval "db.dropDatabase()"`)
  }
  return port
}

const doExecSync = (command, options = {}) => {
  console.log('doExec', command, options)
  return execSync(command, options)
}

const doSpawnAsync = (command, args, options) => {
  console.log('doSpawnAsync', command, options)
  // options = Object.assign(options, { detached: true, stdio: 'ignore' })
  options = Object.assign(options, { detached: true })
  const subprocess = spawn(command, args, options)
  subprocess.stdout.on('data', function(data) {
    console.log(data.toString())
  })
  subprocess.unref()
  // console.log({subprocess}, subprocess.pid)
  return subprocess.pid
}

const getFreePort = () => {
  const store = getStore()
  const values = store && Object.values(store)
  if(!values || !values.length) return process.env.PORT_MIN
  for (let i = +process.env.PORT_MIN; i <= +process.env.PORT_MAX; i++) {
    const processDescription = values.find(p => p.port === i.toString())
    if (!processDescription) return i.toString()
  }
  return null
}

const getDbName = branch => `${process.env.DB_PREFIX}${branch.replace(/\//g, '_')}`